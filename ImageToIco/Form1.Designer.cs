﻿namespace ImageToIco
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            button1 = new Button();
            c16 = new CheckBox();
            c32 = new CheckBox();
            c64 = new CheckBox();
            c128 = new CheckBox();
            c256 = new CheckBox();
            csum = new CheckBox();
            textBox2 = new TextBox();
            label1 = new Label();
            checkBox1 = new CheckBox();
            checkBox2 = new CheckBox();
            listBox1 = new ListBox();
            button2 = new Button();
            button3 = new Button();
            button4 = new Button();
            button5 = new Button();
            pictureBox1 = new PictureBox();
            csvg = new CheckBox();
            c48 = new CheckBox();
            ((System.ComponentModel.ISupportInitialize)pictureBox1).BeginInit();
            SuspendLayout();
            // 
            // button1
            // 
            button1.Location = new Point(596, 359);
            button1.Margin = new Padding(4, 3, 4, 3);
            button1.Name = "button1";
            button1.Size = new Size(75, 30);
            button1.TabIndex = 0;
            button1.Text = "确定输出";
            button1.UseVisualStyleBackColor = true;
            button1.Click += Button1_Click;
            // 
            // c16
            // 
            c16.AutoSize = true;
            c16.Location = new Point(513, 238);
            c16.Margin = new Padding(4, 3, 4, 3);
            c16.Name = "c16";
            c16.Size = new Size(61, 21);
            c16.TabIndex = 2;
            c16.Text = "16x16";
            c16.UseVisualStyleBackColor = true;
            // 
            // c32
            // 
            c32.AutoSize = true;
            c32.Location = new Point(513, 265);
            c32.Margin = new Padding(4, 3, 4, 3);
            c32.Name = "c32";
            c32.Size = new Size(61, 21);
            c32.TabIndex = 3;
            c32.Text = "32x32";
            c32.UseVisualStyleBackColor = true;
            // 
            // c64
            // 
            c64.AutoSize = true;
            c64.Checked = true;
            c64.CheckState = CheckState.Checked;
            c64.Location = new Point(596, 238);
            c64.Margin = new Padding(4, 3, 4, 3);
            c64.Name = "c64";
            c64.Size = new Size(61, 21);
            c64.TabIndex = 4;
            c64.Text = "64x64";
            c64.UseVisualStyleBackColor = true;
            // 
            // c128
            // 
            c128.AutoSize = true;
            c128.Checked = true;
            c128.CheckState = CheckState.Checked;
            c128.Location = new Point(596, 265);
            c128.Margin = new Padding(4, 3, 4, 3);
            c128.Name = "c128";
            c128.Size = new Size(75, 21);
            c128.TabIndex = 5;
            c128.Text = "128x128";
            c128.UseVisualStyleBackColor = true;
            // 
            // c256
            // 
            c256.AutoSize = true;
            c256.Checked = true;
            c256.CheckState = CheckState.Checked;
            c256.Location = new Point(596, 292);
            c256.Margin = new Padding(4, 3, 4, 3);
            c256.Name = "c256";
            c256.Size = new Size(75, 21);
            c256.TabIndex = 6;
            c256.Text = "256x256";
            c256.UseVisualStyleBackColor = true;
            c256.CheckedChanged += c256_CheckedChanged;
            // 
            // csum
            // 
            csum.AutoSize = true;
            csum.Checked = true;
            csum.CheckState = CheckState.Checked;
            csum.Location = new Point(596, 319);
            csum.Margin = new Padding(4, 3, 4, 3);
            csum.Name = "csum";
            csum.Size = new Size(76, 21);
            csum.TabIndex = 7;
            csum.Text = "合集Icon";
            csum.UseVisualStyleBackColor = true;
            // 
            // textBox2
            // 
            textBox2.Location = new Point(74, 11);
            textBox2.Margin = new Padding(4, 3, 4, 3);
            textBox2.Name = "textBox2";
            textBox2.Size = new Size(397, 23);
            textBox2.TabIndex = 8;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new Point(13, 14);
            label1.Margin = new Padding(4, 0, 4, 0);
            label1.Name = "label1";
            label1.Size = new Size(56, 17);
            label1.TabIndex = 9;
            label1.Text = "输出路径";
            // 
            // checkBox1
            // 
            checkBox1.AutoSize = true;
            checkBox1.Location = new Point(513, 15);
            checkBox1.Margin = new Padding(4, 3, 4, 3);
            checkBox1.Name = "checkBox1";
            checkBox1.Size = new Size(75, 21);
            checkBox1.TabIndex = 10;
            checkBox1.Text = "目标目录";
            checkBox1.UseVisualStyleBackColor = true;
            checkBox1.CheckedChanged += CheckBox1_CheckedChanged;
            // 
            // checkBox2
            // 
            checkBox2.AutoSize = true;
            checkBox2.Checked = true;
            checkBox2.CheckState = CheckState.Checked;
            checkBox2.Location = new Point(596, 15);
            checkBox2.Margin = new Padding(4, 3, 4, 3);
            checkBox2.Name = "checkBox2";
            checkBox2.Size = new Size(75, 21);
            checkBox2.TabIndex = 11;
            checkBox2.Text = "子文件夹";
            checkBox2.UseVisualStyleBackColor = true;
            // 
            // listBox1
            // 
            listBox1.FormattingEnabled = true;
            listBox1.ItemHeight = 17;
            listBox1.Location = new Point(12, 41);
            listBox1.Margin = new Padding(4, 3, 4, 3);
            listBox1.Name = "listBox1";
            listBox1.Size = new Size(493, 344);
            listBox1.TabIndex = 12;
            // 
            // button2
            // 
            button2.Location = new Point(513, 359);
            button2.Margin = new Padding(4, 3, 4, 3);
            button2.Name = "button2";
            button2.Size = new Size(75, 30);
            button2.TabIndex = 13;
            button2.Text = "清除全部";
            button2.UseVisualStyleBackColor = true;
            button2.Click += Button2_Click;
            // 
            // button3
            // 
            button3.Location = new Point(513, 202);
            button3.Margin = new Padding(4, 3, 4, 3);
            button3.Name = "button3";
            button3.Size = new Size(75, 30);
            button3.TabIndex = 14;
            button3.Text = "添加文件";
            button3.UseVisualStyleBackColor = true;
            button3.Click += Button3_Click;
            // 
            // button4
            // 
            button4.Location = new Point(596, 202);
            button4.Margin = new Padding(4, 3, 4, 3);
            button4.Name = "button4";
            button4.Size = new Size(75, 30);
            button4.TabIndex = 15;
            button4.Text = "添加目录";
            button4.UseVisualStyleBackColor = true;
            button4.Click += Button4_Click;
            // 
            // button5
            // 
            button5.Location = new Point(479, 11);
            button5.Margin = new Padding(4, 3, 4, 3);
            button5.Name = "button5";
            button5.Size = new Size(26, 23);
            button5.TabIndex = 16;
            button5.Text = "···";
            button5.UseVisualStyleBackColor = true;
            button5.Click += Button5_Click;
            // 
            // pictureBox1
            // 
            pictureBox1.Image = (Image)resources.GetObject("pictureBox1.Image");
            pictureBox1.Location = new Point(513, 42);
            pictureBox1.Name = "pictureBox1";
            pictureBox1.Size = new Size(159, 154);
            pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;
            pictureBox1.TabIndex = 17;
            pictureBox1.TabStop = false;
            // 
            // csvg
            // 
            csvg.AutoSize = true;
            csvg.Checked = true;
            csvg.CheckState = CheckState.Checked;
            csvg.Location = new Point(513, 319);
            csvg.Margin = new Padding(4, 3, 4, 3);
            csvg.Name = "csvg";
            csvg.Size = new Size(72, 21);
            csvg.TabIndex = 18;
            csvg.Text = "输出Svg";
            csvg.UseVisualStyleBackColor = true;
            // 
            // c48
            // 
            c48.AutoSize = true;
            c48.Location = new Point(513, 292);
            c48.Margin = new Padding(4, 3, 4, 3);
            c48.Name = "c48";
            c48.Size = new Size(61, 21);
            c48.TabIndex = 19;
            c48.Text = "48x48";
            c48.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            AutoScaleDimensions = new SizeF(7F, 17F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(684, 401);
            Controls.Add(c48);
            Controls.Add(csvg);
            Controls.Add(pictureBox1);
            Controls.Add(button5);
            Controls.Add(button4);
            Controls.Add(button3);
            Controls.Add(button2);
            Controls.Add(listBox1);
            Controls.Add(checkBox2);
            Controls.Add(checkBox1);
            Controls.Add(label1);
            Controls.Add(textBox2);
            Controls.Add(csum);
            Controls.Add(c256);
            Controls.Add(c128);
            Controls.Add(c64);
            Controls.Add(c32);
            Controls.Add(c16);
            Controls.Add(button1);
            FormBorderStyle = FormBorderStyle.FixedSingle;
            Icon = (Icon)resources.GetObject("$this.Icon");
            Margin = new Padding(4, 3, 4, 3);
            MaximizeBox = false;
            Name = "Form1";
            Text = "图片转ICO工具";
            ((System.ComponentModel.ISupportInitialize)pictureBox1).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private Button button1;
        private CheckBox c16;
        private CheckBox c32;
        private CheckBox c64;
        private CheckBox c128;
        private CheckBox c256;
        private CheckBox csum;
        private TextBox textBox2;
        private Label label1;
        private CheckBox checkBox1;
        private CheckBox checkBox2;
        private ListBox listBox1;
        private Button button2;
        private Button button3;
        private Button button4;
        private Button button5;
        private PictureBox pictureBox1;
        private CheckBox csvg;
        private CheckBox c48;
    }
}