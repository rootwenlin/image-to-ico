using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Xml;
using System.Xml.Linq;

namespace ImageToIco;

public partial class Form1 : Form
{
    public Form1()
    {
        InitializeComponent();
        this.Text = $"图片转ICO工具 森云科技出品 V{System.Reflection.Assembly.GetExecutingAssembly().GetName().Version}";
        this.AllowDrop = true;
        this.DragDrop += Form1_DragDrop;
        this.DragEnter += Form1_DragEnter;
        this.listBox1.SelectedIndexChanged += ListBox1_SelectedIndexChanged;
    }

    private void ListBox1_SelectedIndexChanged(object? sender, EventArgs e)
    {
        if (listBox1.SelectedItem is ListBoxItem item)
        {
            var b = File.ReadAllBytes(item.Text);
            var m = new MemoryStream(b);
            var image = Image.FromStream(m);
            pictureBox1.Image = image;
        }
    }

    private void Form1_DragEnter(object? sender, DragEventArgs e)
    {
        if (e.Data.GetDataPresent(DataFormats.FileDrop))
        {
            e.Effect = DragDropEffects.Link;
        }
        else
        {
            e.Effect = DragDropEffects.None;
        }
    }

    public List<ListBoxItem> items = new List<ListBoxItem>();
    public class ListBoxItem
    {
        public ListBoxItem(string text, bool state = false)
        {
            State = state;
            Text = text;
        }

        public bool State { get; set; }
        public string Text { get; set; }
        public override string ToString()
        {
            return (State ? "完成 " : "导入 ") + Text;
        }
    }

    private void AddListBoxItem(string path)
    {
        if (items.Exists(x => x.Text == path))
            return;
        var item = new ListBoxItem(path);
        items.Add(item);
        listBox1.Items.Add(item);
    }
    private void Form1_DragDrop(object? sender, DragEventArgs e)
    {
        var s = (Array)e.Data.GetData(DataFormats.FileDrop);
        if (s.Length > 0)
        {
            foreach (var item in s)
            {
                if (Directory.Exists(item.ToString()))
                {
                    var ss = Directory.GetFiles(item.ToString(), "*.*").Where(x => x.EndsWith(".png") || x.EndsWith(".jpg")).ToList();
                    foreach (var f in ss)
                    {
                        AddListBoxItem(f);
                    }
                }
                else if (File.Exists(item.ToString()))
                {
                    if (Path.GetExtension(item.ToString()) == ".jpg" || Path.GetExtension(item.ToString()) == ".png")
                        AddListBoxItem(item.ToString());
                }
            }
        }
    }

    private async void Button1_Click(object sender, EventArgs e)
    {
        var result = await Task.Run(() =>
        {
            if (!checkBox1.Checked && !CheckFolder())
            {
                return false;
            }

            for (int i = 0; i < items.Count; i++)
            {
                StartToIco(items[i].Text);
                this.Invoke(new Action(() =>
                {
                    items[i].State = true;
                    listBox1.Items[i] = items[i];
                    listBox1.SelectedIndex = i;
                }));
            }
            return true;

        });
        if (result)
            MessageBox.Show("输出完成");
    }

    private bool CheckFolder()
    {
        if (!checkBox1.Checked)
        {
            string folder = textBox2.Text;
            if (string.IsNullOrEmpty(folder))
            {
                MessageBox.Show("目录不能为空");
                return false;
            }

            if (!Directory.Exists(folder))
            {
                if (MessageBox.Show("文件夹不存在是否创建", "提醒", MessageBoxButtons.OKCancel) == DialogResult.OK)
                    Directory.CreateDirectory(folder);
                else
                    return false;
            }
            textBox2.Text = folder;
        }
        return true;
    }

    private void StartToIco(string path)
    {
        string folder = textBox2.Text;
        if (checkBox1.Checked)
        {
            this.Invoke(new Action(() =>
            {
                textBox2.Text = folder = Path.GetDirectoryName(path);
            }));
        }

        if (c16.Checked)
            StartToIco(path, folder, 16);
        if (c32.Checked)
            StartToIco(path, folder, 32);
        if (c48.Checked)
            StartToIco(path, folder, 48);
        if (c64.Checked)
            StartToIco(path, folder, 64);
        if (c128.Checked)
            StartToIco(path, folder, 128);
        if (c256.Checked)
            StartToIco(path, folder, 256);


        if (csum.Checked)
        {
            string iconpath = Path.Combine(folder, "Icon");
            if (!Directory.Exists(iconpath))
                Directory.CreateDirectory(iconpath);
            var name = Path.GetFileNameWithoutExtension(path);
            ConvertImageToIcon(path, Path.Combine(iconpath, name + ".ico"));
        }

        if (csvg.Checked)
        {
            string svgpath = Path.Combine(folder, "SVG");
            if (!Directory.Exists(svgpath))
                Directory.CreateDirectory(svgpath);
            var name = Path.GetFileNameWithoutExtension(path);
            GetXmlDocumentSvg(path).Save(Path.Combine(svgpath, name + ".svg"));
        }

    }

    private void StartToIco(string path, string saveFolder, int d)
    {
        var name = Path.GetFileNameWithoutExtension(path);

        Size size = new Size(d, d);
        string outPath = $"{saveFolder}\\{name}_{d}x{d}.ico";
        if (checkBox2.Checked)
        {
            string z = $"{saveFolder}\\{d}x{d}";
            if (!Directory.Exists(z))
                Directory.CreateDirectory(z);
            outPath = $"{z}\\{name}_{d}x{d}.ico";
        }
        ConvertImageToIcon(path, outPath, size);
    }


    public static bool ConvertImageToIcon(string origin, string destination, Size iconSize)
    {
        if (iconSize.Width > 512 || iconSize.Height > 512)
        {
            return false;
        }
        using var bitmap = new Bitmap(new Bitmap(origin), iconSize); //先读取已有的图片为bitmap，并缩放至设定大小
        using var stream = new MemoryStream(); //存原图的内存流
        using var iconStream = new MemoryStream(); //存图标的内存流
        bitmap.Save(stream, System.Drawing.Imaging.ImageFormat.Png); //将原图读取为png格式并存入原图内存流
        using var iconWriter = new BinaryWriter(iconStream); //新建二进制写入器以写入目标图标内存流

        //头部  6字节
        iconWriter.Write((short)0); //0-1保留 
        iconWriter.Write((short)1); //2-3文件类型。1=图标, 2=光标
        iconWriter.Write((short)1); //4-5图像数量（图标可以包含多个图像） 单尺寸图标 数量1

        //目录  16字节
        iconWriter.Write((byte)bitmap.Width);//6图标宽度
        iconWriter.Write((byte)bitmap.Height);//7图标高度
        iconWriter.Write((short)0);//8颜色数（若像素位深>=8，填0。这是显然的，达到8bpp的颜色数最少是256，byte不够表示）
        iconWriter.Write((short)0);//9保留。必须为0
        iconWriter.Write((short)0); //12-13位深//10-11调色板
        iconWriter.Write((int)stream.Length); //14-17位图数据大小
        iconWriter.Write(6 + 16);//18-21位图数据起始字节

        //写入图像体至目标图标内存流
        iconWriter.Write(stream.ToArray());

        //保存流，并将流指针定位至头部以Icon对象进行读取输出为文件
        iconWriter.Flush();
        iconWriter.Seek(0, SeekOrigin.Begin);

        using var fileStream = new FileStream(destination, FileMode.Create);
        fileStream.Write(stream.ToArray(), 0, (int)stream.Length);

        //验证文件
        return File.Exists(destination);
    }


    public static bool ConvertImageToIcon(string origin, string destination)
    {
        var s = new int[] { 16, 32, 48, 64, 96, 128, 256 };

        var stream = new MemoryStream(); //存图标的内存流
        var bw = new BinaryWriter(stream); //新建二进制写入器以写入目标图标内存流

        //头部 6字节
        bw.Write((short)0); //0-1保留 
        bw.Write((short)1);//2-3文件类型。1=图标, 2=光标
        bw.Write((short)s.Length);//4-5图像数量（图标可以包含多个图像）

        //目录
        var tbs = new List<byte[]>();
        foreach (var z in s)
        {
            //获取图档数据
            //using var bitmap = new Bitmap(new Bitmap(origin, true), new Size(z, z)); //先读取已有的图片为bitmap，并缩放至设定大小
            using var bitmap = GetBitmap(origin, new Size(z, z));

            using var bitms = new MemoryStream(); //存原图的内存流
            bitmap.Save(bitms, System.Drawing.Imaging.ImageFormat.Png); //将原图读取为png格式并存入原图内存流


            //生成目录 单个目录 16字节
            bw.Write((byte)bitmap.Width);//长
            bw.Write((byte)bitmap.Height);//宽
            bw.Write((short)0);//颜色数，表示颜色数超过256
            bw.Write((short)0);//保留字段，必须为0
            bw.Write((short)1);//颜色平面数或位深，一般为1
            bw.Write((int)bitms.Length);//图标数据长度
            bw.Write(6 + s.Length * 16 + tbs.Sum(it => it.Length));//图标数据偏移量，以字节为单位

            //存储各种图标数组
            tbs.Add(bitms.ToArray());
        }
        foreach (var item in tbs)
        {
            bw.Write(item);
        }

        //保存流，并将流指针定位至头部以Icon对象进行读取输出为文件
        bw.Flush();
        bw.Seek(0, SeekOrigin.Begin);


        var fileStream = new FileStream(destination, FileMode.Create);
        fileStream.Write(stream.ToArray(), 0, (int)stream.Length);
        fileStream.Dispose();

        return File.Exists(destination);
    }

    private static Bitmap GetBitmap(string path, Size size)
    {
        using var image = Image.FromFile(path);
        var bmp = new Bitmap(size.Width, size.Height, PixelFormat.Format24bppRgb);
        bmp.SetResolution(image.HorizontalResolution, image.VerticalResolution);
        bmp.MakeTransparent();
        using var g = Graphics.FromImage(bmp);
        g.SmoothingMode = SmoothingMode.AntiAlias;  //使绘图质量最高，即消除锯齿
        g.InterpolationMode = InterpolationMode.HighQualityBicubic;
        g.CompositingQuality = CompositingQuality.HighQuality;
        g.DrawImage(image, new Rectangle(0, 0, size.Width, size.Height));
        return bmp;
    }


    private static XmlDocument GetXmlDocumentSvg(string path)
    {
        using var image = Image.FromFile(path);
        using var bitms = new MemoryStream(); //存原图的内存流
        image.Save(bitms, System.Drawing.Imaging.ImageFormat.Png); //将原图读取为png格式并存入原图内存流

        var svgDoc = new XmlDocument();
        var svgElement = svgDoc.CreateElement("svg");
        svgElement.SetAttribute("xmlns", "http://www.w3.org/2000/svg");
        svgElement.SetAttribute("width", $"{image.Width}px");
        svgElement.SetAttribute("height", $"{image.Height}px");

        var imageElement = svgDoc.CreateElement("image");
        imageElement.SetAttribute("x", "0");
        imageElement.SetAttribute("y", "0");
        imageElement.SetAttribute("width", $"{image.Width}px");
        imageElement.SetAttribute("height", $"{image.Height}px");
        imageElement.SetAttribute("href", "data:image/png;base64," + Convert.ToBase64String(bitms.ToArray()));
        svgElement.AppendChild(imageElement);
        svgDoc.AppendChild(svgElement);
        return svgDoc;
    }


    private void CheckBox1_CheckedChanged(object sender, EventArgs e)
    {
        if (checkBox1.Checked)
        {
            textBox2.ReadOnly = true;
            button5.Enabled = false;
            textBox2.Text = "";
        }
        else
        {
            textBox2.ReadOnly = false;
            button5.Enabled = true;
        }
    }

    private void Button2_Click(object sender, EventArgs e)
    {
        items.Clear();
        listBox1.Items.Clear();
    }

    private void Button3_Click(object sender, EventArgs e)
    {
        var ofd = new OpenFileDialog
        {
            Filter = "(*.png图档)|*.png|(*.jpg图档)|*.jpg"
        };
        if (ofd.ShowDialog() == DialogResult.OK)
        {
            AddListBoxItem(ofd.FileName);
        }
    }

    private void Button4_Click(object sender, EventArgs e)
    {
        var dialog = new FolderBrowserDialog();
        if (dialog.ShowDialog() == DialogResult.OK)
        {
            var ss = Directory.GetFiles(dialog.SelectedPath, "*.*").Where(x => x.EndsWith(".png") || x.EndsWith(".jpg")).ToList();
            foreach (var f in ss)
            {
                AddListBoxItem(f);
            }
        }
    }

    private void Button5_Click(object sender, EventArgs e)
    {
        var dialog = new FolderBrowserDialog();
        if (dialog.ShowDialog() == DialogResult.OK)
        {
            textBox2.Text = dialog.SelectedPath;
        }
    }

    private void c256_CheckedChanged(object sender, EventArgs e)
    {

    }
}